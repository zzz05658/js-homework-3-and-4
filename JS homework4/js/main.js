function createNewUser(){
    let firstName = prompt('Enter your FirstName:');
    let lastName = prompt('Enter your LastName');

    const newUser = {
        firstName,
        lastName,
        setFirstName(value){
            Object.defineProperty(newUser, 'firstName',{
                writable: true,});
            return this.firstName = value;
        },
        setLastName(value){
            Object.defineProperty(newUser, 'lastName',{
                writable: true,});
            return this.lastName = value;
        },
        getLogin() {
            let firstLetter = firstName.charAt(0).toLocaleLowerCase();
            lastName = lastName.toLocaleLowerCase();
            return firstLetter + lastName;
        }
    };
    Object.defineProperty(newUser,"lastName",{
        writable: false,
    });
    Object.defineProperty(newUser,"firstName",{

        writable: false,
    });
    return newUser;
}
const newUser = createNewUser();
console.log(newUser.getLogin());